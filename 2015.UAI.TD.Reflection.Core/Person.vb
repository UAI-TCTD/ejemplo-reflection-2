﻿Public Class Person
    Event GotEmail(ByVal msg As String, ByVal priority As Integer)
    ' Some public and private fields
    Public FirstName As String
    Public LastName As String
    Dim m_Age As Short
    Dim m_EmailAddress(4) As String

    Sub New()
    End Sub



    ReadOnly Property Name
        Get
            Return FirstName + " " + LastName
        End Get
    End Property

    Sub New(ByVal FirstName As String, ByVal LastName As String)
        Me.FirstName = FirstName
        Me.LastName = LastName
    End Sub

    Property Age() As Short
        Get
            Return m_Age
        End Get
        Set(ByVal Value As Short)
            m_Age = Value
        End Set
    End Property

    Property EmailAddress(ByVal Index As Short) As String
        Get
            Return m_EmailAddress(Index)
        End Get
        Set(ByVal Value As String)
            m_EmailAddress(Index) = Value
        End Set
    End Property

    Sub SendEmail(ByVal msg As String, Optional ByVal Priority As Integer = 1)

        Console.WriteLine("Message to " & FirstName & " " & LastName)
        Console.WriteLine("Priority = " & Priority.ToString)
        Console.WriteLine(msg)
        RaiseEvent GotEmail(msg, Priority)
    End Sub
End Class