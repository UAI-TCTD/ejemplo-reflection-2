﻿Imports System.Reflection
'Imports System



Public Class AssemblySamples
    Public Property Nombre As String()
    Public Sub RetrievingTypeAttributes()

        'Obtiene una Referencia en donde este código se está ejecutando
        Dim asm As [Assembly] = Assembly.GetExecutingAssembly()

        Dim rootDomianPath = Environment.CurrentDirectory



        'Obtener una Referencia a un Ensamblado dada su ubicación
        Dim asmFromLocation As Assembly = Assembly.LoadFrom(rootDomianPath + "\\2015.UAI.TD.Reflection.Core.dll")

        'Dado su Display Name
        Dim asmDisplayName As Assembly = Assembly.Load("2015.UAI.TD.Reflection.Core")

        Dim myType2 As Type = GetType(String)
        Console.WriteLine("Leer miembros de un tipo")
        Console.WriteLine("-------------------------------")
        For Each t As Type In asm.GetExportedTypes()
            If t.IsClass Then
                Console.WriteLine(t.Name & " (Class)")
            ElseIf t.IsEnum Then
                
                Console.WriteLine(t.Name & " (Enum)")
            ElseIf t.IsValueType Then
                Console.WriteLine(t.Name & " (Structure)")
            ElseIf t.IsInterface Then
                Console.WriteLine(t.Name & " (Interface)")

            ElseIf t.IsAbstract Then
                Console.Write("MustInherit")
            ElseIf t.IsSealed Then
                Console.Write("NotInheritable")

                ' We need this test because System.Object has no base class.
            ElseIf Not t.BaseType Is Nothing Then
                Console.WriteLine("(base: " & t.BaseType.FullName & ") ")

            Else
                ' This statement is never reached because a type
                ' can’t be something other than one of the above.
            End If

            

            For Each fi As FieldInfo In t.GetFields()
                If (fi.IsPublic Or fi.IsAssembly) And Not fi.IsLiteral Then
                    Console.WriteLine("{0} As {1}", fi.Name, fi.FieldType.Name)
                End If
            Next

            For Each mi As MethodInfo In t.GetMethods()
                If mi.IsFinal Then
                    Console.Write("NotOverridable ")
                ElseIf mi.IsVirtual Then
                    Console.Write("Overridable ")
                ElseIf mi.IsAbstract Then
                    Console.Write("MustOverride ")
                End If
                Dim retTypeName As String = mi.ReturnType.FullName
                If retTypeName = "System.Void" Then

                    Console.WriteLine("Sub {0}", mi.Name)
                Else
                    Console.WriteLine("Function {0} As {1}", mi.Name, retTypeName)
                End If
            Next

            For Each pi As PropertyInfo In t.GetProperties
                ' Get either the Get or the Set accessor methods
                Dim mi As MethodInfo, modifier As String = ""
                If pi.CanRead Then
                    mi = pi.GetGetMethod
                    If Not pi.CanWrite Then modifier = "ReadOnly "
                Else
                    mi = pi.GetSetMethod()
                    modifier = "WriteOnly "
                End If
                ' Display only Public and Protected properties.
                If mi.IsPublic Or mi.IsFamily Then
                    Console.WriteLine("{0}{1} As {2}", modifier, pi.Name, _
                    pi.PropertyType.FullName)
                End If
            Next
        Next
    End Sub


    Public Sub InvokingMembers()

        Console.WriteLine("Escribir o invocar miembros de un tipo")
        Console.WriteLine("-------------------------------")
        ' Create a Person object and reflect on it.
        Dim pers As New Person("Joe", "Doe")
        Dim persType As Type = pers.GetType()



        'escribir field
        ' Get a reference to its FirstName field.
        Dim fi As FieldInfo = persType.GetField("FirstName")
        ' Display its current value then change it.
        Console.WriteLine(fi.GetValue(pers)) ' => Joe
        fi.SetValue(pers, "Robert")
        ' Prove that it changed.
        Console.WriteLine(pers.FirstName) ' => Robert

        'escribir property
        ' Get a reference to the PropertyInfo object.
        Dim pi As PropertyInfo = persType.GetProperty("Age")
        ' Note that the type of value must match exactly.
        ' (Integer constants must be converted to Short, in this case.)
        pi.SetValue(pers, 35S, Nothing)
        ' Read it back.
        Console.WriteLine(pi.GetValue(pers, Nothing)) ' => 35


        'invocar metodos
        ' Get the MethodInfo for this method.
        Dim mi As MethodInfo = persType.GetMethod("SendEmail")
        ' Prepare an array for expected arguments.
        Dim args() As Object = {"This is a message", 3}
        ' Invoke the method.
        mi.Invoke(pers, args)

    End Sub
End Class
