﻿
Public Class Persona

    Public FirstName As String
    Public LastName As String

    Sub New(ByVal FirstName As String, ByVal LastName As String)
        Me.FirstName = FirstName
        Me.LastName = LastName
    End Sub

    <SaludableAttribute("hola")>
    Public Function Saludar()
        Return FirstName + " " + LastName
    End Function


    <SaludableAttribute("hola")>
    Public Function Saludar2()
        Return "luis"
    End Function

End Class