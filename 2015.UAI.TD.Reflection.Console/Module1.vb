﻿Imports _2015.UAI.TD.Reflection.Core
Imports System.Reflection
Imports System.Attribute

Module Module1

    Sub Main()
        Dim sample As New AssemblySamples
        sample.RetrievingTypeAttributes()
        sample.InvokingMembers()


        Dim p As New Persona("pepe", "argento")


        Dim unMetodo As MethodInfo = GetType(Persona).GetMethod("Saludar2")

        Dim Attributo As SaludableAttribute = CType(Attribute.GetCustomAttribute(unMetodo, GetType(SaludableAttribute)), SaludableAttribute)

        If Not Attributo Is Nothing Then
            System.Console.WriteLine(String.Format("{0} {1}", Attributo.Saludo, p.Saludar))
        End If

      

        System.Console.ReadKey()
    End Sub

End Module
