﻿<AttributeUsage(AttributeTargets.Method, AllowMultiple:=True)>
Public Class SaludableAttribute
    Inherits System.Attribute



    Private _saludo As String




    Public ReadOnly Property Saludo() As String
        Get
            Return _saludo
        End Get
    End Property


    Sub New(ByVal saludo As String)
        _saludo = saludo

    End Sub


End Class
